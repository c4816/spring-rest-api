# Rest API

## Deploy
---
activate profile windows  
`$ set spring_profile_active=<profile>`

run  
`$ mvnw spring-boot:run`

## Build
---
clean target  
`$ mvnw clean`

build  
`$ mvnw package`

## Change profile (ter atenção ao nome do application.properties)
---
`$ mvnw spring-boot:run -Dspring-boot.run.profiles=dev`
`$ java -jar target/rest_api-<version>.jar --spring.profiles.active=production`

## Docker build
---
`git clone https://gitlab.com/c4816/spring-rest-api.git`
`cd spring-rest-api`
`git pull`
`$ docker build -t spring-rest-api:latest .`
`docker run -it --name spring-rest-api --rm -p 80:8090 spring-rest-api:latest`

## CI/CD
---
`$ sudo apk add gitlab-runner`
sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "HGxCsZ3znbsfCRWSFvHT" \
  --executor "shell" \
  --description "docker-lab" \
  --tag-list "develop_server_tag" \
  --run-untagged="false" \
  --locked="false" \
  --access-level="not_protected"

  sudo gitlab-runner register \
  --non-interactive \
  --url "https://gitlab.com/" \
  --registration-token "HGxCsZ3znbsfCRWSFvHT" \
  --executor "shell" \
  --description "docker-lab-node2" \
  --tag-list "production_server_tag" \
  --run-untagged="false" \
  --locked="false" \
  --access-level="not_protected"