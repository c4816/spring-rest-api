package pt.rumos.rest_api.users;

import java.util.List;

import com.google.gson.Gson;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class userController {
    @Autowired
    private userService service;

    @GetMapping("/user")
    public String getAllUsers(Model m) {
        List<user> entities = service.findAll();
        return new Gson().toJson(entities);
    }

}
