package pt.rumos.rest_api.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import java.util.List;


@Service
public class userService {
    @Autowired
    private userRepository repo;

    @ModelAttribute("users")
    public List<user> findAll() {
        return (List<user>) repo.findAll();
    }
}
