package pt.rumos.rest_api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class restApiController {
    @Autowired
    restApiProps props;

    @Autowired
    restApiProfile prof;

    @RequestMapping("/")
    public String index(Model m) {
        m.addAttribute("name", props.getName());
        m.addAttribute("version", props.getVersion());
        m.addAttribute("description", props.getDescription());
        m.addAttribute("url", props.getUrl());
        m.addAttribute("profile", prof.getActiveProfile());
        return "index";
    }
}
